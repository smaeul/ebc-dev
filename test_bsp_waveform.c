// SPDX-License-Identifier: GPL-2.0+

#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "compat.h"
#include "fileio.h"

#define BSP
#include "waveform.h"

#define PVI_WF_FILE_SIZE		0x200000
#define PVI_WF_LUT_FRAME_SIZE		(0x100 * 0x100)
#define PVI_WF_LUT_SIZE			(0x100 * PVI_WF_LUT_FRAME_SIZE)

static int pvi_wf_test(const char *in_file, const char *out_dir)
{
	struct epd_lut_data ctx = {};
	enum epd_lut_type lut_type;
	char out_path[PATH_MAX];
	size_t wf_file_size = 0;
	int ret, temperature;
	void *wf_file;

	ctx.wf_table = malloc(PVI_WF_LUT_SIZE);
	if (!ctx.wf_table)
		return ENOMEM;

	ret = readfile(in_file, &wf_file, &wf_file_size);
	if (ret)
		return ret;
	pr_info("TEST: read %zu bytes from '%s'\n\n", wf_file_size, in_file);

	ret = pvi_wf_input(wf_file);
	if (ret < 0)
		pr_err("TEST: failed to load waveform file: %d\n", ret);

	for (lut_type = WF_TYPE_RESET; lut_type < WF_TYPE_MAX; ++lut_type) {
		for (temperature = 0; temperature < 50; temperature += 3) {
			ctx.frame_num = 0;
			pr_info(">>>> pvi_wf_get_lut(ctx, %s, %d)\n",
				epd_lut_type_names[lut_type], temperature);
			ret = pvi_wf_get_lut(&ctx, lut_type, temperature);
			ctx.frame_num &= 0xff;
			pr_info("<<<< %d\n\n", ret);
			if (ret || !ctx.frame_num)
				break;

			snprintf(out_path, sizeof(out_path),
				 "%s/lut_%s_%d.bin",
				 out_dir, epd_lut_type_names[lut_type], temperature);
			ret = writefile(out_path, ctx.wf_table,
					ctx.frame_num * PVI_WF_LUT_FRAME_SIZE);
			if (ret) {
				pr_err("TEST: failed to write output file: %d\n", ret);
				break;
			}
		}
	}

	return 0;
}

int main(int argc, char *argv[])
{
	if (argc < 3) {
		pr_err("usage: %s <waveform.bin> <out_dir>\n", argv[0]);
		return 111;
	}

	return pvi_wf_test(argv[1], argv[2]);
}
