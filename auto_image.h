// SPDX-License-Identifier: GPL-2.0+

enum panel_refresh_mode {
	EPD_AUTO		= 0,
	EPD_OVERLAY		= 1,
	EPD_FULL_GC16		= 2,
	EPD_FULL_GL16		= 3,
	EPD_FULL_GLR16		= 4,
	EPD_FULL_GLD16		= 5,
	EPD_FULL_GCC16		= 6,
	EPD_PART_GC16		= 7,
	EPD_PART_GL16		= 8,
	EPD_PART_GLR16		= 9,
	EPD_PART_GLD16		= 10,
	EPD_PART_GCC16		= 11,
	EPD_A2			= 12,
	EPD_A2_DITHER		= 13,
	EPD_DU			= 14,
	EPD_DU4			= 15,
	EPD_A2_ENTER		= 16,
	EPD_RESET		= 17,
	EPD_SUSPEND		= 18,
	EPD_RESUME		= 19,
	EPD_POWER_OFF		= 20,
	EPD_FORCE_FULL		= 21,
};

#ifdef BSP

struct atomic_t {
	int counter;
};

typedef struct atomic64_t {
	long counter;
} atomic_long_t;

struct list_head {
	struct list_head *next;
	struct list_head *prev;
};

struct hlist_node {
	struct hlist_node *next;
	struct hlist_node **pprev;
};

typedef struct qspinlock {
	union {
		struct atomic_t val;
		struct {
			u8 locked;
			u8 pending;
		};
		struct {
			u16 locked_pending;
			u16 tail;
		};
	};
} arch_spinlock_t;

struct raw_spinlock {
	arch_spinlock_t raw_lock;
};

typedef struct spinlock {
	struct raw_spinlock rlock;
} spinlock_t;

struct timer_list {
	struct hlist_node entry;
	ulong expires;
	void (*function)(struct timer_list *);
	u32 flags;
	u64 android_kabi_reserved1;
	u64 android_kabi_reserved2;
};

struct wakeup_source {
	char *name;
	int id;
	struct list_head entry;
	spinlock_t lock;
	struct wake_irq *wakeirq;
	struct timer_list timer;
	ulong timer_expires;
	ktime_t total_time;
	ktime_t max_time;
	ktime_t last_time;
	ktime_t start_prevent_time;
	ktime_t prevent_sleep_time;
	ulong event_count;
	ulong active_count;
	ulong relax_count;
	ulong expire_count;
	ulong wakeup_count;
	struct device *dev;
	bool active:1;
	bool autosleep_enabled:1;
};

struct wake_lock {
	struct wakeup_source ws;
};

struct work_struct {
	atomic_long_t data;
	struct list_head entry;
	void (*func)(struct work_struct *);
	u64 android_kabi_reserved1;
	u64 android_kabi_reserved2;
};

struct epd_lut_data {
	uint frame_num;
	uint *data;
	u8 *wf_table;
};

struct panel_buffer {
	void * virt_addr;
	ulong phy_addr;
	size_t size;
};

struct ebc_info {
	ulong ebc_buffer_phy;
	char *ebc_buffer_vir;
	int ebc_buffer_size;
	int ebc_buf_real_size;
	int direct_buf_real_size;
	ulong lut_buffer_phy;
	int lut_buffer_size;
	int is_busy_now;
	char frame_total;
	char frame_bw_total;
	int auto_need_refresh;
	int frame_left;
	int part_mode_count;
	int full_mode_num;
	int height;
	int width;
	int *lut_addr;
	int buf_align16;
	int ebc_irq_status;
	int ebc_dsp_buf_status;
	struct device *dev;
	struct epd_lut_data lut_data;
	struct task_struct *ebc_task;
	int *auto_image_new;
	int *auto_image_old;
	int *auto_image_bg;
	u8 *auto_frame_count;
	u8 *auto_image_osd;
	void *direct_buffer[2];
	int ebc_power_status;
	int ebc_last_display;
	char *lut_ddr_vir;
	struct ebc_buf_s *prev_dsp_buf;
	struct ebc_buf_s *curr_dsp_buf;
	struct wake_lock suspend_lock;
	int wake_lock_is_set;
	int first_in;
	struct timer_list vdd_timer;
	struct timer_list frame_timer;
	struct work_struct auto_buffer_work;
	int is_early_suspend;
	int is_deep_sleep;
	int is_power_off;
	int overlay_enable;
	int overlay_start;
};

struct ebc_panel {
	struct device *dev;
	struct ebc_tcon *tcon;
	struct ebc_pmic *pmic;
	struct panel_buffer fb[2];
	int current_buffer;
	u32 width;
	u32 height;
	u32 vir_width;
	u32 vir_height;
	u32 width_mm;
	u32 height_mm;
	u32 direct_mode;
	u32 sdck;
	u32 lsl;
	u32 lbl;
	u32 ldl;
	u32 lel;
	u32 gdck_sta;
	u32 lgonl;
	u32 fsl;
	u32 fbl;
	u32 fdl;
	u32 fel;
	u32 panel_16bit;
	u32 panel_color;
	u32 mirror;
};

struct ebc {
	struct device *dev;
	struct ebc_tcon *tcon;
	struct ebc_pmic *pmic;
	struct ebc_panel panel;
	struct ebc_info info;
};

extern struct ebc *global_ebc;

void get_auto_image(u8 *data_buf, u8 *buffer_new, u8 *buffer_old,
		    u8 *frame_count, struct ebc_info *ebc_info);

void refresh_new_image2(u8 *image_new, u8 *image_fb, u8 *image_bg,
			u8 *frame_count, struct ebc_info *ebc_info, int buf_mode);

#else

#include "waveform.h"

struct ebc_ctx {
	u8 *fb_image;		// active framebuffer, exposed to userspace, etc.
	u8 *new_image;		// displayed image at end of waveform
	u8 *old_image;		// displayed image at beginning of waveform
	u8 *frame_count;	// frames remaining in this waveform
	u32 width;		// in pixels
	u32 height;		// in pixels
};

int ebc_framebuffer_damage(struct ebc_ctx* ebc_ctx, struct pvi_wf_ctx *wf_ctx);

int ebc_generate_auto_frame(struct ebc_ctx* ebc_ctx, struct pvi_wf_ctx *wf_ctx,
			    u8* output);

#endif
