# SPDX-License-Identifier: GPL-2.0+

WAVEFORM_FILE	?= ../waveform.bin
OUTPUT_DIR	?= out

CC		 = aarch64-linux-musl-gcc -static
CFLAGS		+= -g -O2 -std=gnu89 -Wall -Werror -Wextra \
		   -Wno-unused-function -Wno-unused-parameter
CPPFLAGS	+= -Ilib
LDFLAGS		+= -Wl,--gc-sections

images		 = $(foreach i,1 2 3 4,images/pinenotebg$(i).raw)

bin-y += test_bsp_auto_image
bin-y += test_bsp_waveform
bin-y += test_new_auto_image
bin-y += test_new_waveform

test_bsp_auto_image-y	+= lib/fileio.o lib/compat.o bsp/ebc_dev_v8.o
test_bsp_waveform-y	+= lib/fileio.o lib/compat.o bsp/pvi_waveform_v8.o
test_new_auto_image-y	+= lib/fileio.o auto_image.o
test_new_waveform-y	+= lib/fileio.o waveform.o

obj-y += $(foreach bin,$(bin-y),$(bin).o $($(bin)-y))

all: $(bin-y)

check: check_auto_image check_waveform

check_%: $(OUTPUT_DIR)/.check_%.stamp;

clean:
	rm -f $(bin-y) $(obj-y)
	rm -rf $(OUTPUT_DIR)

images/%.raw: images/%.png
	convert $< -depth 4 -rotate 270 gray:$@

$(OUTPUT_DIR)/.check_%.stamp: $(OUTPUT_DIR)/bsp_% $(OUTPUT_DIR)/new_%
	diff -qr $^
	touch $@

$(OUTPUT_DIR)/%_auto_image: test_%_auto_image $(images) | $(OUTPUT_DIR)/new_waveform
	rm -rf $@
	qemu-aarch64 $< $(OUTPUT_DIR)/new_waveform/lut_WF_TYPE_AUTO_24.bin $@ $(images)

$(OUTPUT_DIR)/%_waveform: test_%_waveform
	rm -rf $@
	qemu-aarch64 $< $(WAVEFORM_FILE) $@

$(foreach bin,$(bin-y),$(eval $(bin): $($(bin)-y)))

.PHONY: all check check_% clean
.SECONDARY:
