// SPDX-License-Identifier: GPL-2.0+

#include "compat.h"

#include "auto_image.h"

#define LOG2_PIXELS_PER_U64		4
#define PIXELS_PER_U64			(1U << LOG2_PIXELS_PER_U64)

int ebc_framebuffer_damage(struct ebc_ctx* ebc_ctx, struct pvi_wf_ctx *wf_ctx)
{
	u64 *fb_image = (u64 *)ebc_ctx->fb_image;
	u64 *new_image = (u64 *)ebc_ctx->new_image;
	u8 *frame_count = ebc_ctx->frame_count;
	u64 fb_pixels, new_pixels, offset, pixel;
	int col, cols = DIV_ROUND_UP(ebc_ctx->width, PIXELS_PER_U64);
	int row, rows = ebc_ctx->height;

	// refresh_new_image2
	// This loop only needs to run for damaged areas of the framebuffer!
	for (row = 0; row < rows; ++row) {
		for (col = 0; col < cols; ++col) {
			offset = row * cols + col;

			fb_pixels = fb_image[offset];
			new_pixels = new_image[offset];

			for (pixel = 0; pixel < PIXELS_PER_U64; ++pixel) {
				u64 fb_pixel = (fb_pixels >> (4 * pixel)) & 0xf;
				u64 new_pixel = (new_pixels >> (4 * pixel)) & 0xf;
				u8 frames_left = frame_count[PIXELS_PER_U64 * offset + pixel];

				// If an update is already in progress, wait until it completes.
				if (frames_left)
					continue;

				if (fb_pixel == 0xe)
					fb_pixel = 0xf;

				// If the display already matches the framebuffer, there's nothing to do.
				if (new_pixel == fb_pixel)
					continue;

				// Otherwise, update the display and initialize the frame counter.
				new_image[offset] &= ~(0xfULL << (4 * pixel));
				new_image[offset] |= fb_pixel << (4 * pixel);
				frame_count[PIXELS_PER_U64 * offset + pixel] = wf_ctx->frame_num;
			}
		}
	}

	return 0;
}

/*
 * Differences between the referesh modes:
 *
 * EPD_OVERLAY: Runs all of the code below.
 * EPD_AUTO: Runs everything from EPD_OVERLAY, minus the background image mask.
 * EPD_*_PART: Runs everything from EPD_AUTO, except uses a global frame count.
 * EPD_*_FULL: Like _PART, but never skips pixels, and out_byte starts as 0xff.
 *
 */
int ebc_generate_auto_frame(struct ebc_ctx* ebc_ctx, struct pvi_wf_ctx *wf_ctx,
			    u8* output)
{
	u64 *new_image = (u64 *)ebc_ctx->new_image;
	u64 *old_image = (u64 *)ebc_ctx->old_image;
	u8 *frame_count = ebc_ctx->frame_count;
	const u8 *wf_table = wf_ctx->wf_table;
	u64 new_pixels, offset, old_pixels, pixel;
	int col, cols = DIV_ROUND_UP(ebc_ctx->width, PIXELS_PER_U64);
	int row, rows = ebc_ctx->height;
	int ret = false;

	// get_auto_image, ignoring mirroring
	for (row = 0; row < rows; ++row) {
		for (col = 0; col < cols; ++col) {
			offset = row * cols + col;

			output[4 * offset + 0] = 0;
			output[4 * offset + 1] = 0;
			output[4 * offset + 2] = 0;
			output[4 * offset + 3] = 0;

			old_pixels = old_image[offset];
			new_pixels = new_image[offset];

			// Vectorize this!
			for (pixel = 0; pixel < PIXELS_PER_U64; ++pixel) {
				u64 old_pixel = (old_pixels >> (4 * pixel)) & 0xf;
				u64 new_pixel = (new_pixels >> (4 * pixel)) & 0xf;
				u8 frames_left = frame_count[PIXELS_PER_U64 * offset + pixel];

				if (!frames_left)
					continue;

				// refresh is needed
				ret = true;

				u8 this_frame = wf_ctx->frame_num - frames_left;
				u8 out_byte = wf_table[wf_table_cell(this_frame, old_pixel, new_pixel)] & 0x3;

				output[4 * offset + pixel / 4] |= out_byte << (2 * (pixel % 4));

				// If the remaining frame count is zero, sync the new image to the old image.
				if (!--frames_left) {
					old_image[offset] &= ~(0xfULL << (4 * pixel));
					old_image[offset] |= new_pixel << (4 * pixel);
				}
				ebc_ctx->frame_count[PIXELS_PER_U64 * offset + pixel] = frames_left;
			}
		}
	}

	return ret;
}
