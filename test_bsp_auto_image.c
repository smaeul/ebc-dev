// SPDX-License-Identifier: GPL-2.0+

#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "compat.h"
#include "fileio.h"

#define BSP
#include "auto_image.h"

#define IMAGE_HEIGHT	1404
#define IMAGE_WIDTH	1872

#define FRAME_CNT_SIZE	((IMAGE_WIDTH * IMAGE_HEIGHT) / 1) // 8 bpp
#define DATA_BUF_SIZE	((IMAGE_WIDTH * IMAGE_HEIGHT) / 4) // 2 bpp
#define IMAGE_BUF_SIZE	((IMAGE_WIDTH * IMAGE_HEIGHT) / 2) // 4 bpp

static int auto_image_test(const char *lut_file, const char *out_dir,
			   const char *images[], int image_count)
{

	int frame, image, image_frames, lut_frames, ret, total_frames;
	void *bg, *data_buf, *frame_count, *new_buf, *old_buf;
	void *image_buf = NULL, *lut = NULL;
	size_t image_size = 0, lut_size = 0;
	char out_path[PATH_MAX];
	struct ebc ebc = {};

	bg		= malloc(IMAGE_BUF_SIZE);
	data_buf	= malloc(DATA_BUF_SIZE);
	frame_count	= malloc(FRAME_CNT_SIZE);
	new_buf		= malloc(IMAGE_BUF_SIZE);
	old_buf		= malloc(IMAGE_BUF_SIZE);

	if (!data_buf || !frame_count || !new_buf || !old_buf)
		return ENOMEM;

	ret = readfile(lut_file, &lut, &lut_size);
	if (ret)
		return ret;
	lut_frames = lut_size / 0x10000;

	ebc.info.lut_data.frame_num	= lut_frames;
	ebc.info.lut_data.wf_table	= lut;

	ebc.info.frame_total		= ebc.info.lut_data.frame_num;
	ebc.info.height			= IMAGE_HEIGHT;
	ebc.info.width			= IMAGE_WIDTH;

	ebc.panel.mirror		= 0;
	ebc.panel.vir_width		= IMAGE_WIDTH;
	ebc.panel.vir_height		= IMAGE_HEIGHT;

	global_ebc = &ebc;

	// Run the test for several sets of full/partial updates.
	for (image_frames = lut_frames; image_frames > 1; image_frames /= 3) {
		pr_info("> Starting run with %d/%d frames per image\n",
			image_frames, lut_frames);

		// Clear out state between test runs.
		memset(bg, 0xff, IMAGE_BUF_SIZE);
		memset(data_buf, 0, DATA_BUF_SIZE);
		memset(frame_count, 0, FRAME_CNT_SIZE);
		memset(new_buf, 0xff, IMAGE_BUF_SIZE);
		memset(old_buf, 0xff, IMAGE_BUF_SIZE);
		total_frames = 0;

		// Loop through the series of test images.
		for (image = 0; image < image_count; ++image) {
			pr_info(">> Switching to image %d/%d (%s)\n",
				image + 1, image_count, images[image]);

			ret = readfile(images[image], &image_buf, &image_size);
			if (ret)
				return ret;

			// Run the auto image algorithm and record its output.
			for (frame = 0; frame < image_frames || image == image_count - 1; ++frame) {
				++total_frames;

				// Copy the image into the "new" buffer.
				refresh_new_image2(new_buf, image_buf, bg,
						   frame_count, &ebc.info, EPD_AUTO);

				// Generate the waveform outputs.
				get_auto_image(data_buf, new_buf, old_buf,
					       frame_count, &ebc.info);

				snprintf(out_path, sizeof(out_path),
					 "%s/auto_test_%d/image_%d/frame_%03d.bin",
					 out_dir, image_frames, image, frame);
				ret = writefile(out_path, data_buf, DATA_BUF_SIZE);
				if (ret) {
					pr_err("TEST: failed to write output file: %d\n", ret);
					break;
				}

				if (!ebc.info.auto_need_refresh) {
					pr_info("<<< Auto complete in %d frames!\n", frame);
					break;
				} else if (frame >= 0x100) {
					pr_err("Not complete after %d frames!\n", frame);
					break;
				}
			}

			pr_info("<< Finished image %d/%d\n",
				image + 1, image_count);
		}

		pr_info("< Finished run in %d frames!\n",
			total_frames);
	}

	return 0;
}

int main(int argc, const char *argv[])
{
	if (argc < 4) {
		pr_err("usage: %s <lut.bin> <out_dir> <image...>\n", argv[0]);
		return 111;
	}

	return auto_image_test(argv[1], argv[2], argv + 3, argc - 3);
}
