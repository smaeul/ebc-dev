# Reimplementation of Rockchip E-Ink waveform/LUT code

The goal of this project is to create a human-readable, FLOSS implementation of
the data path from the frame buffer to the E-Ink hardware. Correctness of the
initial version is verified by comparing with the pre-compiled BSP code, by
running both codebases in qemu-user.

## Dependencies

* An AArch64 Linux cross toolchain
* Qemu (to run the compiled binaries)
* ImageMagick (to convert PNG images to raw grayscale)
* A "PVI" waveform file (e.g. extracted from the PineNote firmware).

## How to build/run

Type `make check`. **WARNING** This will produce about 2 GiB of binary output
files. You should point `OUTPUT_DIR` to a `tmpfs` so you do not kill your disk.

## How to build a display pipeline

1) Map two buffers for writing to the E-Ink display.
2) Map 3x framebuffers (actual framebuffer, plus new/old buffers).
3) Read the LUT data from disk.
4) Wait for framebuffer updates. When one occurs (and no display update is
   already in progress):
   1) Update the LUT based on the current mode and temperature (`waveform.c`).
   2) Fill up one output buffer and send it to the TCON (`auto_image.c`).
   3) Trigger the first frame (see below).
   4) Fill up the second buffer (`auto_image.c`).
   5) Arm the frame complete IRQ.
5) When a frame complete IRQ occurs:
   1) Flip buffers, sending the previously-filled frame (see below).
   2) Fill in the now-free buffer with the next frame (`auto_image.c`).
   3) If no more frames are needed, disarm the frame complete IRQ.

## How to send a frame to the display

```
dma_sync_single_for_device(phy_addr_of_buffer);
tcon->dsp_mode_set(tcon, 0, 0, 0, 0);
tcon->image_addr_set(tcon, phy_addr_of_buffer, 0);
tcon->frame_start(tcon, 1);
```
