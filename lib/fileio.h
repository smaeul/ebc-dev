// SPDX-License-Identifier: GPL-2.0+

int readfile(const char *path, void **buf, size_t *buf_size);
int writefile(const char *path, void *buf, size_t buf_size);
