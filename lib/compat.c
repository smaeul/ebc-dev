// SPDX-License-Identifier: GPL-2.0+

#include <stdarg.h>

#include "compat.h"

int printk(const char *fmt, ...)
{
	va_list ap;
	int ret;

	va_start(ap, fmt);
	ret = vprintf(fmt, ap);
	va_end(ap);

	return ret;
}
