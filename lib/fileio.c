// SPDX-License-Identifier: GPL-2.0+

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "fileio.h"

int readfile(const char *path, void **buf, size_t *buf_size)
{
	size_t file_size;
	ssize_t nread;
	int fd, ret;
	off_t off;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return errno;

	off = lseek(fd, 0, SEEK_END);
	if (off < 0) {
		ret = errno;
		goto err;
	}

	file_size = off;
	if (file_size > *buf_size) {
		*buf = realloc(*buf, file_size);
		if (!*buf) {
			ret = ENOMEM;
			goto err;
		}
	}
	*buf_size = file_size;

	nread = pread(fd, *buf, file_size, 0);
	if (nread != off) {
		ret = errno;
		goto err;
	}

	ret = 0;

err:
	close(fd);

	return ret;
}

int writefile(const char *path, void *buf, size_t buf_size)
{
	ssize_t nwritten;
	char *local_path, *slash;
	int fd, ret;

	local_path = slash = strdup(path);
	if (!local_path)
		return ENOMEM;
	while ((slash = strchr(slash + 1, '/'))) {
		*slash = '\0';
		mkdir(local_path, 0777);
		*slash = '/';
	}
	free(local_path);

	fd = open(path, O_CREAT | O_TRUNC | O_WRONLY, 0666);
	if (fd < 0)
		return errno;

	nwritten = write(fd, buf, buf_size);
	if (nwritten != (ssize_t)buf_size) {
		ret = errno;
		goto err;
	}

	ret = 0;

err:
	close(fd);

	return ret;
}
