// SPDX-License-Identifier: GPL-2.0+

#include <stdint.h>
#include <stdio.h>

#include <linux/errno.h>

#define ARRAY_SIZE(a)			(sizeof (a) / sizeof ((a)[0]))

#define DIV_ROUND_UP(n, d)		(((n) + (d) - 1) / (d))

#define pr_err(...)			printf(__VA_ARGS__)
#define pr_info(...)			printf(__VA_ARGS__)

#define false				0
#define true				1

typedef  uint8_t u8;
typedef uint16_t __le16;
typedef uint16_t u16;
typedef uint32_t __le32;
typedef uint32_t u32;
typedef  int64_t s64;
typedef uint64_t u64;

typedef unsigned char bool;
typedef unsigned int  uint;
typedef unsigned long ulong;

typedef s64 ktime_t;
